package tampon;

import common.Message;

public class Tampon {

	/*
	 * TODO: 1.Ajouter une socket qui accepte les connexions. 2. Ajouter un
	 * premier traitement de message, le décoder pour savoir si c'est une
	 * requete (Entete REQ) ou un message de producteur (Entete MSG). Suivant
	 * l'entete, renvoyer le message dans sur_reception_de ou sur_demande_de. 3.
	 * Renvoyer la réponse correcte à l'interlocuteur (rejet ou acquittement
	 * pour Prod, rejet ou Message pour Consomm). 4. Du coup, on a un thread par
	 * site connecté. Prévoir la gestion de ça.
	 */

	/** Le tampon à proprement parlé. Type de gestion: circulaire. **/
	Message[] tampon;
	private int pointeurEcriture;
	private int pointeurLecture;

	/**
	 * Initialisation de Tampon avec la taille spécifiée, les indices buffer à
	 * 0.
	 * 
	 * @param tailleBuffer
	 *            Taille de tampon utilisé.
	 */
	public Tampon(int tailleBuffer) {
		tampon = new Message[tailleBuffer];
		pointeurEcriture = 0;
		pointeurLecture = 0;
	}

	/**
	 * Sur réception de message de producteur, retourner vrai si il y a de la
	 * place et stocker le message dans le buffer, faux sinon
	 * 
	 * @param message
	 *            Message reçu d'un producteur
	 * @return Vrai si le buffer n'est pas plein, faux sinon.
	 */
	boolean sur_reception_de(Message message) {
		synchronized (tampon) {
			if (tampon[pointeurEcriture] == null) {
				tampon[pointeurEcriture] = message;
				System.out.println("Nouveau message dans le buffer");
				printbuffer();
				pointeurEcriture = incrementerPointeur(pointeurEcriture);
				return true;
			} else
				System.out.println("Buffer plein");
				return false;
		}
	}

	/**
	 * Sur demande d'un consommateur, retourner un message si le tampon n'est
	 * pas vide. Retourner null sinon.
	 * 
	 * @return Message si le tampon n'est pas vide, null sinon.
	 */
	Message sur_demande_de() {
		synchronized (tampon) {
			if (tampon[pointeurLecture] == null){
				System.out.println("Pas de messages dans le buffer");
				return new Message("RJT", "Pas de messages");
			}
			else {
				Message mess = tampon[pointeurLecture];
				tampon[pointeurLecture] = null;
				System.out.println("Retourne un message du buffer");
				printbuffer();
				pointeurLecture = incrementerPointeur(pointeurLecture);
				return mess;
			}
		}
	}
	
	/**
	 * Permet de visualizer l'état du buffer
	 */
	private void printbuffer() {
		synchronized(tampon){
			StringBuffer str = new StringBuffer();
			for(int i = 0; i< tampon.length; i++){
				str.append("[" + ((tampon[i]==null) ? "0" : "1") + "]");
			}
			System.out.println(str);
		}
	}

	/**
	 * Implémentation de l'indice de buffer circulaire.
	 * 
	 * @param pointeur
	 *            Valeur de pointeur actuelle
	 * @return Valeur correcte après incrémentation : 0 si l'indice était
	 *         maximal, i++ sinon
	 */
	private int incrementerPointeur(int pointeur) {
		if (pointeur == tampon.length - 1)
			return 0;
		else
			return ++pointeur;
	}
}
