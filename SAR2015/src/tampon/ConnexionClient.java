package tampon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import common.Encodeur;
import common.Message;

public class ConnexionClient extends Thread {

	private BufferedReader socketReader;
	private PrintWriter socketWriter;
	private Tampon parent;

	Socket client;

	public ConnexionClient(Socket cl, Tampon parent) {

		this.client = cl;
		this.parent = parent;
		try {
			socketWriter = new PrintWriter(client.getOutputStream());
			socketReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		Message reponse;
		Message message;
		while (true) {
			reponse = null;
			message = new Message("FLT", "Erreur");
			try {
				String messageRecu = socketReader.readLine();
				if (messageRecu != null) {
					message = Encodeur.decoder(messageRecu);
					System.out.println("Type message: " + message.getEntete());
				} else {
					message = new Message("FLT", " ");
				}

				if (message.getEntete().contains("RQT")) {
					reponse = parent.sur_demande_de();
				} else if (message.getEntete().contains("MSG")) {
					boolean aquittement = parent.sur_reception_de(message);
					Message messageReponse;
					if (aquittement) {
						System.out.println("Acquittement positif");
						reponse = new Message("ACK", "VIDE");
					}
				}

				if (reponse == null) {
					reponse = new Message("RJT", "Message illisible");
				}

				System.out.println("Réponse:" + reponse.getEntete());
				socketWriter.println(Encodeur.encoder(reponse));
				socketWriter.flush();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
