package tampon;

import java.net.ServerSocket;
import java.net.Socket;

import common.Donnees;

public abstract class MainT {
	
	public static void main(String[] args) {
		int clients = 0;
		Tampon t = new Tampon(Donnees.TAILLE_BUFFER);
		ServerSocket sv;
		try {
			sv = new ServerSocket(Donnees.PORT_TAMPON);
			while (true) {
				Socket client;
				System.out.println("En attente de clients...");
				client = sv.accept();
				System.out.println(++clients + " clients connectés");
				
				new ConnexionClient(client,t).start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}