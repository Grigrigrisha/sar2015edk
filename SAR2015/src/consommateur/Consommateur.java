package consommateur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import common.Donnees;
import common.Encodeur;
import common.Message;
import producteur.Producteur;

public class Consommateur extends Thread{
	
	String name;
	
	/**
	 * Comptabilisons le nombre de refus que le consommateur a reçu.
	 */
	int refus;
	int messagesConsommes;
	
	static int CONSOMMATEURS = 0;
	int id;
	private int port;
	private BufferedReader socketReader;
	private PrintWriter socketWriter;
	
	private int delaiMinMSec;
	private int delaiMaxMSec;

	
	
	
	Consommateur(){
		
		
		this.id = Consommateur.CONSOMMATEURS;
		Consommateur.CONSOMMATEURS++;
		
		this.port = Donnees.PORT_PREMIER_CONSOMMATEUR;
		Donnees.PORT_PREMIER_CONSOMMATEUR++;
		
		this.delaiMinMSec = Donnees.DELAI_MIN_MSEC;
		this.delaiMaxMSec = Donnees.DELAI_MAX_MSEC;
		
		refus = 0;
		messagesConsommes = 0;
	}
	
	public void run(){
		try {
			Socket clientSocket = new Socket("localhost", Donnees.PORT_TAMPON);
			socketWriter = new PrintWriter(clientSocket.getOutputStream());
			socketReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while(true){
				try {
					Thread.sleep(delaiAttente());
					consommer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
		} catch (UnknownHostException e) {
			System.out.println("Impossible de déterminer l'hôte poour le producteur " + id);
		} catch (IOException e) {
			System.out.println("Un problème est survenu à la connexion du producteur " + id);
			try {
				Thread.sleep(3000);
				this.run();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
				System.exit(0);
			}
		}
	}
	
	private void consommer() {
		socketWriter.println(Encodeur.encoder(new Message("RQT"," ")));
		socketWriter.flush();
		try {
			String reception = socketReader.readLine();
			Message m = Encodeur.decoder(reception);
			if(m.getEntete().contains("RJT")){
				System.out.println("Consommateur " + this.getId() + " a reçu un refus. Total: " + refus++);
			}
			else if(m.getEntete().contains("MSG")){
				System.out.println("Consommateur " + this.getId() + " a consommé le message " + m.getCorps() + ". Total: " + messagesConsommes++);
			}
			else{
				System.out.println("Consommateur " + this.getId() + " a reçu un message illisible de tampon");
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private int delaiAttente() {
		Random rand = new Random();
		int delai = (rand.nextInt(delaiMaxMSec/100 - delaiMinMSec/100 + 1) + delaiMinMSec/100)*100;
		System.out.println("Delai aléatoire demandé est de " + delai + "ms");
		return delai;
	}
}
