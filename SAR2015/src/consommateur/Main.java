package consommateur;

import common.Donnees;

public class Main {

	public static void main(String[] args) {
		for(int i=0; i<Donnees.NOMBRE_PRODUCTEURS; i++){
			new Consommateur().start();
		}
	}

}
