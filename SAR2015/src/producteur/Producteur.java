package producteur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import common.Donnees;
import common.Encodeur;
import common.Message;

/**Le site producteur construit et envoit des messages à des intervalles irréguliers*/
public class Producteur extends Thread{
	
	public static int PRODUCTEURS = 0;
	public int id;
	private int port;
	private BufferedReader socketReader;
	private PrintWriter socketWriter;
	
	private int delaiMinMSec;
	private int delaiMaxMSec;
	
	int refus;
	int messagesProduits;
	
	Producteur(){
		this.id = Producteur.PRODUCTEURS;
		Producteur.PRODUCTEURS++;
		
		this.port = Donnees.PORT_PREMIER_PRODUCTEUR;
		Donnees.PORT_PREMIER_PRODUCTEUR++;
		
		this.delaiMinMSec = Donnees.DELAI_MIN_MSEC;
		this.delaiMaxMSec = Donnees.DELAI_MAX_MSEC;
		
		refus = 0;
		messagesProduits = 0;
	}
	
	public void run(){
		
		try {
			Socket clientSocket = new Socket("localhost", Donnees.PORT_TAMPON);
			socketWriter = new PrintWriter(clientSocket.getOutputStream());
			socketReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while(true){
				try {
					Thread.sleep(delaiAttente());
					System.out.println("Début envoi de message");
					produire(new Message("MSG","De " + this.id));
					System.out.println("Envoi de message");
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
		} catch (UnknownHostException e) {
			System.out.println("Impossible de déterminer l'hôte poour le producteur " + id);
		} catch (IOException e) {
			System.out.println("Un problème est survenu à la connexion du producteur " + id);
			try {
				Thread.sleep(3000);
				this.run();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
				System.exit(0);
			}
		}
	}
	
	/** Réalise directement les fonctions de la couche service, soit envoyer le message au Tampon
	 * et recevoir la réponse.*/
	private void produire(Message message) {
		socketWriter.println(Encodeur.encoder(message));
		socketWriter.flush();
		try {
			String reception = socketReader.readLine();
			Message receptionM = Encodeur.decoder(reception);
			System.out.println(reception + " : reçu de T");
			if(receptionM.getEntete().contains("ACK")){
				System.out.println("Producteur " + this.getId() + " a reçu un acquittement de tampon, TOTAL: " + messagesProduits++);
			}
			else if(receptionM.getEntete().contains("RJT")){
				System.out.println("Producteur " + this.getId() + " a reçu un rejet de tampon, TOTAL: " + refus++);
			}
			else{
				System.out.println("Producteur " + this.getId() + " a reçu un message illisible de tampon");
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private int delaiAttente() {
		Random rand = new Random();
		int delai = (rand.nextInt((delaiMaxMSec/100 - delaiMinMSec/100) + 1) + delaiMinMSec/100)*100;
		System.out.println("Delai aléatoire demandé est de " + delai + "ms");
		return delai;
	}
}
