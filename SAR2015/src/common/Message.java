package common;

public class Message {
	String entete;
	String corps;

	public Message(String entete, String corps){
		this.entete = entete;
		this.corps = corps;
	}
	
	public String getEntete(){
		return entete;
	}
	
	public String getCorps(){
		return corps;
	}
}
