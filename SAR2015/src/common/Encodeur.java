package common;

/**Claisse abstraite accessible à tous les sites, 
 * qui implémente le protocole d'encodage/décodage pour les messages.*/
public abstract class Encodeur {
	
	/**
	 * Encode la chaine de caractères afin que celle-ci soit envoyée sur le réseau.
	 * @param m
	 * @return
	 */
	public static String encoder(Message m){
		return m.getEntete() + "::" + m.getCorps();
	}
	
	/**
	 * Décode la chaine de caractères reçue depuis le socket.
	 * @param str
	 * @return
	 */
	public static Message decoder(String str){
		if(str.split("::").length > 1){
			String entete = str.split("::")[0];
			String corps = str.split("::")[1];
			return new Message(entete,corps);
		}
		else
			return new Message("FLT","str");
	}
}
