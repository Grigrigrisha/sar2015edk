package common;

/**
 * Toutes les donnees parametrables de programme
 * @author GrigoryKrnlv
 *
 */
public abstract class Donnees{
	public static int PORT_TAMPON = 10101;
	public static int PORT_PREMIER_PRODUCTEUR = 10201;
	public static int PORT_PREMIER_CONSOMMATEUR = 10301;
	
	public static int TAILLE_BUFFER = 8;
	
	/** Délai minimal de production/consommation */
	public static int DELAI_MIN_MSEC = 500;
	
	/**Délai maximal de production/consommation */
	public static int DELAI_MAX_MSEC = 3000;
	
	/**Nombre de producteurs à lancer **/
	public static int NOMBRE_PRODUCTEURS = 3;
	
	/**Nombre de consommateurs ) lancer **/
	public static int NOMBRE_CONSOMMATEURS = 5;
}
