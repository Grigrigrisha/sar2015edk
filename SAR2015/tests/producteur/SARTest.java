package producteur;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import common.Donnees;

public class SARTest {

	@Test
	public void test() {
		int min = Donnees.DELAI_MIN_MSEC;
		int max = Donnees.DELAI_MAX_MSEC;
		
		for(int i = 0; i < 10000; i++){
			Random rand = new Random();
			//int randomNum = rand.nextInt((max - min) + 1) + min;
			int random = (rand.nextInt(max/100 - min/100 + 1) + min/100)*100;
			System.out.println(random);
			if(random < min || random > max)
				fail("Mauvais range");
		}

	}

}
